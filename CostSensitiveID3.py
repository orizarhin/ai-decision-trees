import ID3
import numpy as np
from collections import Counter
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt

"""
///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////   QUESTION 4   /////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
"""

class PrioritizeLoss(ID3.ClassChooser):
    """
    This class calculates the majority, while giving a bigger weight for the class given for prioritization.
    """
    def __init__(self, classification_to_prioritize='M', weight=10):
        """
        :param classification_to_prioritize: Classification value to prioritize.
        :param weight: The weight with which to prioritize it.
        """
        ID3.ClassChooser.__init__(self)
        self.classification_to_prioritize = classification_to_prioritize
        self.weight = weight

    def choose_classification(self, examples: list):
        """
        Will return a classification, while prioritizing the given class for prioritization.
        :param examples: Examples left in node.
        :return:
        """
        classification_hist = Counter([e.classification for e in examples])
        if len(classification_hist) == 1:
            return classification_hist.most_common()[0][0]

        for k in classification_hist:
            if k == self.classification_to_prioritize:
                priority_num = classification_hist[k]
                priority_val = k
            else:
                dispriority_num = classification_hist[k]
                dispriority_val = k

        if priority_num * self.weight > dispriority_num:
            return priority_val
        else:
            return dispriority_val


class EndWithPositiveWeight(ID3.EndChecker):
    """
    This class will decide to prune while prioritizing loss.
    """

    def __init__(self, m: int, weight=10, positive_classification='M'):
        ID3.EndChecker.__init__(self)
        self.m = m
        self.weight = weight
        self.positive_classification = positive_classification

    def finished(self, examples, features):
        classification_hist = Counter([e.classification for e in examples])

        positive_class_size = 0
        negative_class_size = 0

        for k in classification_hist:
            if k == self.positive_classification:
                positive_class_size = classification_hist[k]
            else:
                negative_class_size = classification_hist[k]

        return len(classification_hist) == 1 or positive_class_size >= negative_class_size * self.weight\
               or len(examples) <= self.m or len(features) == 0


class CostSensitiveID3(ID3.TDIDT):
    def __init__(self, examples: list, features: np.array, pruning_param: int, classification_to_prioritize='M',
                 weight=20):
        ID3.TDIDT.__init__(self, examples=examples,
                           features=features,
                           default_class_chooser=PrioritizeLoss(classification_to_prioritize=classification_to_prioritize,
                                                                weight=weight),
                           feature_chooser=ID3.DynamicContinuousFeaturing(),
                           end_checker=EndWithPositiveWeight(m=pruning_param, weight=weight))


def question_4_3():
    train_examples, np_features = ID3.get_set('train.csv')

    cost_sensitive_id3 = CostSensitiveID3(examples=train_examples,
                                          features=np_features,
                                          pruning_param=ID3.optimal_pruning_parameter,
                                          weight=5)
    classifier = cost_sensitive_id3.create_decision_tree()
    test_examples, _ = ID3.get_set('test.csv')
    ID3.run_test_with_loss(classifier=classifier, test_set=test_examples, test_str="q4.3", positive_val='M',
                           print_only_preciseness=True, print_nothing=False)


def experiment_with_loss(test_str='train.csv', number_of_folds=5, plot_results=True, print_results=False):
    """
    Runs an experiment over the loss parameter.
    :param test_str:                 Path for the test set csv file.
    :param number_of_folds:          Sets the number of different folds we will train and test over.
    :param plot_results:             Bool, marks whether we should use matplotlib to plot the results.
    :param print_results             Bool, marks whether we should print the results in text format.
    :return:
    """
    examples, np_features = ID3.get_set(test_str)

    m_params = np.arange(start=4, stop=30, step=3)

    kfold = KFold(n_splits=number_of_folds, shuffle=True, random_state=313177784)
    m_results_all_folds = []  # Holds all results for each fold.
    i = 1
    for train_index, test_index in kfold.split(examples):
        current_fold_examples = [examples[i] for i in train_index]
        current_fold_tests = [examples[i] for i in test_index]
        m_results_current_fold = []
        for m in m_params:
            cost_sensitive_id3 = CostSensitiveID3(examples=current_fold_examples,
                                                  features=np_features,
                                                  pruning_param=ID3.optimal_pruning_parameter,
                                                  weight=m)
            classifier = cost_sensitive_id3.create_decision_tree()
            loss = ID3.run_test_with_loss(classifier=classifier, test_set=current_fold_tests, print_nothing=True)
            m_results_current_fold.append((m, loss))
        m_results_all_folds.append((i, m_results_current_fold))
        i += 1

    sum_results_all_folds = {}
    for m in m_params:
        sum_results_all_folds[m] = 0

    for fold_res in m_results_all_folds:
        for res_tup in fold_res[1]:
            sum_results_all_folds[res_tup[0]] += res_tup[1]

    average_results_out_of_folds = [(k, (v / number_of_folds)) for k, v in sum_results_all_folds.items()]

    if print_results:
        print("A list of results, where each result is a tuple of (parameter, average preciseness):")
        print(average_results_out_of_folds)

    if plot_results:
        X = []
        Y = []
        for tup in average_results_out_of_folds:
            X.append(tup[0])
            Y.append(tup[1])
        plt.plot(X, Y)
        plt.ylabel('Loss')
        plt.xlabel('Weight')
        plt.show()


#  experiment_with_loss()

if __name__ == '__main__':
    question_4_3()
