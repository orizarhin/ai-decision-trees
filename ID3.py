import numpy as np
import pandas as pd
from collections import Counter
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt


class Example:
    """
    A class that keeps any and all information regarding a given example or test subject.
    :param: classification: holds None in the case it is a test subject, otherwise will include classification.
    :param: features: an np.array holding all features, while each feature is held at the corresponding index.
    """
    def __init__(self, classification, features: np.array):
        self.classification = classification
        self.features = np.array(features)


class Feature:
    """
    An abstract feature class.
    :param: index: The corresponding index for usage in the "Example" class' "features" parameter.
    """
    def __init__(self, index):
        self.index = index

    def check_feature(self, test_subject: Example):
        """
        return true if test_subject fits the feature (this is set as belonging to the "left_sub_tree").
        :param test_subject: class Example. The subject which we want to classify.
        :return: True if the test_subject fits the feature. False otherwise.
        """
        raise NotImplementedError


class ContinuousFeature(Feature):
    """
    A class used in Decision_Tree_Node in order to classify a subject at each node.
    :param: index: The corresponding index for usage in the "Example" class' "features" parameter.
    :param: dividing_value: the value set to be the best divider by the algorithm (Assuming the feature
                            is in fact continuous).
    """
    def __init__(self, index, dividing_value):
        Feature.__init__(self, index=index)
        self.dividing_value = dividing_value

    def check_feature(self, test_subject):
        """
        :param test_subject: class Example.
        :return: True or false, if the test_subject passes the feature.
        """
        return test_subject.features[self.index] < self.dividing_value


class DecisionTreeNode:
    """
    Each node represents a feature that aids in classification.
    :param: self.feature: receives a test_subject as arguement, and returns true if the subject
                          belongs to the left_sub_tree.
    :param: self.left_sub_tree: the left sub tree. Test subject moves here if feature returned true.
    :param: self.right_sub_tree: the right sub tree. Test subject moves here if feature returned false.
    :param: self.classification: If it is None, use feature to continue classiciation. Else, return it.
    """
    def __init__(self, feature, left_sub_tree, right_sub_tree, classification):
        self.feature = feature
        self.left_sub_tree = left_sub_tree
        self.right_sub_tree = right_sub_tree
        self.classification = classification

    def classify(self, test_subject: Example):
        if self.classification is None:
            if self.feature.check_feature(test_subject):
                return self.left_sub_tree.classify(test_subject)
            else:
                return self.right_sub_tree.classify(test_subject)
        else:
            return self.classification


class DecisionTree:
    """
    Decision tree class. Given an test subject, its method "classify" will return a classification.
    """
    def __init__(self, root: DecisionTreeNode):
        self.root = root

    def classify(self, test_subject: Example):
        return self.root.classify(test_subject)


class ClassChooser:
    """
    This class is intended to add some more abstract-ness to this code. Is used to choose a default class for a Node.
    """
    def __init__(self):
        pass

    def choose_classification(self, examples: list):
        raise NotImplementedError


class Majority(ClassChooser):
    """
    This class picks the majority classification from a set of examples.
    """
    def __init__(self):
        ClassChooser.__init__(self)

    def choose_classification(self, examples: list):
        classification_hist = Counter([e.classification for e in examples])
        return classification_hist.most_common()[0][0]


class FeatureChooser:
    """
    This class is intended to add some more abstract-ness to this code. Is used to choose the next feature to be used.
    """
    def __init__(self):
        pass

    def choose_feature(self, examples, features):
        return NotImplementedError


class FirstFeature(FeatureChooser):
    """
    This class chooses the first available feature.
    """
    def __init__(self):
        FeatureChooser.__init__(self)

    def choose_feature(self, examples, features):
        return features[0]


class DynamicContinuousFeaturingWithError(FeatureChooser):
    """
    This class chooses a feature dynamically using error calculation.
    """

    def __init(self):
        FeatureChooser.__init__(self)

    def max_pick_last(self, information_gain_tuples, key, secondary_key):
        last_best = None
        for tup in information_gain_tuples:
            if last_best is None or key(last_best) < key(tup):
                max_inf_gain = key(tup)
                last_best = tup
            if max_inf_gain == key(tup) and secondary_key(last_best) < secondary_key(tup):
                last_best = tup
        return last_best

    def max_pick_first(self, information_gain_tuples, key, secondary_key):
        first_best = None
        for tup in information_gain_tuples:
            if first_best is None or key(first_best) < key(tup):
                max_inf_gain = key(tup)
                first_best = tup
            if max_inf_gain == key(tup) and secondary_key(first_best) > secondary_key(tup):
                first_best = tup
        return first_best

    def calculate_error(self, examples):
        if len(examples) == 0:
            return 0
        classification_hist = Counter([e.classification for e in examples])
        return 1 - (max([(classification_hist[k] / len(examples)) for k in classification_hist]))

    def calculate_error_gain(self, examples, feature_index):
        if len(examples) == 0:
            return 0
        base_error_gain = self.calculate_error(examples=examples)
        error_gain_tuples = []

        sorted_features = sorted([e.features[feature_index] for e in examples])
        dividers = [((sorted_features[i] + sorted_features[i + 1]) / 2) for i in range(len(examples) - 1)]

        for d in dividers:
            under_divider = [e for e in examples if e.features[feature_index] < d]
            over_divider = [e for e in examples if e.features[feature_index] >= d]
            number_of_examples = len(examples)
            error_gain = base_error_gain - ((1 / number_of_examples) *
                                            ((len(under_divider) * self.calculate_error(examples=under_divider)) +
                                             (len(over_divider) * self.calculate_error(examples=over_divider))))
            error_gain_tuples.append((error_gain, d))
        return self.max_pick_first(error_gain_tuples, key=(lambda tup: tup[0]),
                                   secondary_key=(lambda tup: tup[1]))

    def find_best_feature(self, examples, features: np.array):
        information_gain_tuples = []
        for f in features:
            information_gain_tuples.append((f, self.calculate_error_gain(examples=examples, feature_index=f)))
        return self.max_pick_last(information_gain_tuples, key=(lambda tup: tup[1][0]),
                                  secondary_key=(lambda tup: tup[0]))

    def choose_feature(self, examples, features):
        feature_tuple = self.find_best_feature(examples=examples, features=np.sort(features))
        return ContinuousFeature(index=feature_tuple[0], dividing_value=feature_tuple[1][1])


class DynamicContinuousFeaturing(FeatureChooser):
    """
    This class chooses a feature dynamically using information gain.
    """
    def __init(self):
        FeatureChooser.__init__(self)

    def calculate_entropy(self, examples):
        if len(examples) == 0:
            return 0
        entropy = 0
        classification_hist = Counter([e.classification for e in examples])
        for k in classification_hist:
            probability = classification_hist[k] / len(examples)
            entropy += (probability * np.log2(probability))
        return -entropy

    def calculate_information_gain(self, examples, feature_index):
        """
        :param examples: a list of examples given to check IG over.
        :param feature_index: the feature index to check IG for.
        :return: returns a tuple of (information_gain, best_divider).
        """
        if len(examples) == 0:
            return 0
        e_size = len(examples)
        base_entropy = self.calculate_entropy(examples)
        information_gain_tuples = []
        sorted_features = np.sort([e.features[feature_index] for e in examples])
        for d in [((sorted_features[i_e] + sorted_features[i_e + 1]) / 2) for i_e in range(len(examples) - 1)]:
            under_divider = []
            over_divider = []
            temp_feature = ContinuousFeature(index=feature_index, dividing_value=d)
            for e in examples:
                if temp_feature.check_feature(e):
                    under_divider.append(e)
                else:
                    over_divider.append(e)
            under_divider_entropy = self.calculate_entropy(examples=under_divider)
            over_divider_entropy = self.calculate_entropy(examples=over_divider)
            under_divider_percentage = len(under_divider) / e_size
            over_divider_percentage = len(over_divider) / e_size
            information_gain = base_entropy - (under_divider_percentage * under_divider_entropy) -\
                                              (over_divider_percentage * over_divider_entropy)
            information_gain_tuples.append((information_gain, d))
        #  sorted_forwards = sorted(information_gain_tuples, key=(lambda tup: tup[1]))
        return max(information_gain_tuples, key=(lambda tup: tup[0]))

    def find_best_feature(self, examples, features: np.array):
        information_gain_tuples = [(f, self.calculate_information_gain(examples=examples, feature_index=f))
                                   for f in features]
        sorted_backwards = sorted(information_gain_tuples, key=(lambda tup: tup[0]), reverse=True)
        return max(sorted_backwards, key=(lambda tup: tup[1][0]))

    def choose_feature(self, examples, features):
        feature_tuple = self.find_best_feature(examples=examples, features=np.sort(features))
        return ContinuousFeature(index=feature_tuple[0], dividing_value=feature_tuple[1][1])


class EndChecker:
    """
    An abstract class. Its "finished" method receives a list of examples, and decides whether or not it should
    stop at the current node.
    """
    def __init__(self):
        pass

    def finished(self, examples, features):
        raise NotImplementedError


class SingleClassEnd(EndChecker):
    """
    This EndChecker will agree to end the search if a single class has remained in the node or if no features
    are available. This class will obviously cause some overfitting.
    """

    def __init__(self):
        EndChecker.__init__(self)

    def finished(self, examples, features):
        classification_hist = Counter([e.classification for e in examples])
        return len(classification_hist) <= 1 or len(features) == 0


class PruneBySize(EndChecker):
    """
    This EndChecker will agree to end if reached a certain node size or if no features are available.
    """

    def __init__(self, m: int):
        EndChecker.__init__(self)
        self.m = m

    def finished(self, examples, features):
        classification_hist = Counter([e.classification for e in examples])
        return len(classification_hist) == 1 or len(examples) <= self.m or len(features) == 0


class TDIDT:
    """
    Classic TDIDT class, returns a DecisionTree type according to the parameters given to it.
    :param: examples: a list of class Example objects.
    :param: features: an np.array object containing all feature indices
    :param: default_class_chooser: an object of class ClassChooser. Is in charge of choosing a classification
                                   when a leaf is created.
    :param: feature_chooser: an object of class FeatureChooser. Is in charge of choosing a feature for any
                             Node our of all available features.
    :param: end_checker: an object of class EndChecker. Its "finished" method returns True if it is time to
            create a leaf type node.
    """

    def __init__(self, examples: list, features: np.array, default_class_chooser: ClassChooser,
                 feature_chooser: FeatureChooser, end_checker: EndChecker):
        self.examples = examples
        self.features = features
        self.default_class_chooser = default_class_chooser
        self.feature_chooser = feature_chooser
        self.end_checker = end_checker

    def create_decision_tree_leaf(self, examples):
        classification = self.default_class_chooser.choose_classification(examples)
        return DecisionTreeNode(feature=None,
                                left_sub_tree=None,
                                right_sub_tree=None,
                                classification=classification)

    def create_decision_tree_node(self, examples, features):
        if self.end_checker.finished(examples, features):
            return self.create_decision_tree_leaf(examples)

        next_feature = self.feature_chooser.choose_feature(examples=examples, features=features)

        if next_feature is None:
            return self.create_decision_tree_leaf(examples)

        left_examples = []
        right_examples = []

        for e in examples:
            if next_feature.check_feature(e):
                left_examples.append(e)
            else:
                right_examples.append(e)

        if len(left_examples) == 0 or len(right_examples) == 0:
            return self.create_decision_tree_leaf(examples)

        node = DecisionTreeNode(feature=next_feature,
                                left_sub_tree=self.create_decision_tree_node(examples=left_examples,
                                                                             features=np.array(features)),
                                right_sub_tree=self.create_decision_tree_node(examples=right_examples,
                                                                              features=np.array(features)),
                                classification=None)
        return node

    def create_decision_tree(self):
        return DecisionTree(self.create_decision_tree_node(self.examples.copy(), self.features.copy()))


class ID3(TDIDT):
    """
    Classic ID3 implementation, using the abstract TDIDT above.
    :param: examples: a list of class Example objects
    :param: features: an np.array of existing feature indices/keys in Example.features
    """
    def __init__(self, examples: list, features: np.array):
        TDIDT.__init__(self,
                       examples=examples.copy(),
                       features=features.copy(),
                       default_class_chooser=Majority(),
                       feature_chooser=DynamicContinuousFeaturing(),
                       end_checker=SingleClassEnd())


def get_set(str_path):
    examples = []
    dataframe = pd.read_csv(str_path)

    for index, row in dataframe.iterrows():
        classification = row[0]
        np_features = np.array([row[i] for i in range(1, len(row))])
        examples.append(Example(classification=classification,
                                      features=np_features))

    np_features = np.arange(len(row) - 1)
    return examples, np_features


def get_results(classifier: DecisionTree, test_set: list, positive_val='M'):
    correct_classifications = 0
    false_classifications = 0
    correct_positive_classifications = 0
    false_positive_classifications = 0
    correct_negative_classifications = 0
    false_negative_classifications = 0

    for e in test_set:
        classified = classifier.classify(e)
        if classified == e.classification:
            correct_classifications += 1
            if classified == positive_val:
                correct_positive_classifications += 1
            else:
                correct_negative_classifications += 1
        else:
            false_classifications += 1
            if classified == positive_val:
                false_positive_classifications += 1
            else:
                false_negative_classifications += 1
    return (correct_classifications,
            false_classifications,
            correct_positive_classifications,
            false_positive_classifications,
            correct_negative_classifications,
            false_negative_classifications)

def run_test(classifier: DecisionTree, test_set: list, test_str=None, positive_val='M',
             print_only_preciseness=True, print_nothing=False):
    if print_nothing:
        print_only_preciseness = True

    (correct_classifications,
     false_classifications,
     correct_positive_classifications,
     false_positive_classifications,
     correct_negative_classifications,
     false_negative_classifications) = get_results(classifier=classifier,
                                                   test_set=test_set,
                                                   positive_val=positive_val)

    if print_only_preciseness:
        if not print_nothing:
            print(correct_classifications / len(test_set))
        return correct_classifications / len(test_set)

    print("Done.")
    print("")
    print("----------------------------------")
    print("Correctness report -", test_str, ":")
    print("----------------------------------")
    print("- Correct classifications:", correct_classifications)
    print("- False classifications:", false_classifications)
    print("----------------------------------")
    print("- Correct positives:", correct_positive_classifications)
    print("- False positives:", false_positive_classifications)
    print("----------------------------------")
    print("- Correct negatives:", correct_negative_classifications)
    print("- False negatives:", false_negative_classifications)
    print("----------------------------------")
    print("- Preciseness:", correct_classifications / len(test_set))
    print("----------------------------------")
    print("")

    return correct_classifications / len(test_set)


def question_1(dont_print_info=True):
    not dont_print_info and print("Building a training set.")

    train_examples, np_features = get_set('train.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Creating the ID3 class:")

    id3 = ID3(examples=train_examples, features=np_features)

    not dont_print_info and print("Done.")
    not dont_print_info and print("Training a classifier:")

    import time
    start_time = time.time()

    classifier = id3.create_decision_tree()

    not dont_print_info and print("Finished training after", time.time() - start_time, "seconds.")
    not dont_print_info and print("Done.")

    not dont_print_info and print("Building a set of examples from the training group.")

    train_test_examples, _ = get_set('train.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Will now test the training examples.")

    not dont_print_info and run_test(classifier=classifier, test_set=train_test_examples, test_str="training subjects", positive_val='M')

    not dont_print_info and print("Building a set of examples from the test group.")

    test_examples, _ = get_set('test.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Running classifications on all examples...")

    run_test(classifier=classifier, test_set=test_examples, test_str="test subjects", positive_val='M',
             print_only_preciseness=dont_print_info)


"""
///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////   QUESTION 1   /////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
"""

if __name__ == '__main__':
    question_1(dont_print_info=True)

"""
///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////   QUESTION 3   /////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
"""

"""
Optimal pruning parameter, according to the experiment on the training set.
"""
optimal_pruning_parameter = 6

class ID3_Pruning(TDIDT):
    """
    Pruning ID3 implementation, using the abstract TDIDT above.
    :param: examples: a list of class Example objects
    :param: features: an np.array of existing feature indices/keys in Example.features
    :param: pruning_param:
    """
    def __init__(self, examples: list, features: np.array, pruning_param: int):
        TDIDT.__init__(self,
                       examples=examples.copy(),
                       features=features.copy(),
                       default_class_chooser=Majority(),
                       feature_chooser=DynamicContinuousFeaturing(),
                       end_checker=PruneBySize(pruning_param))


def experiment(test_str='train.csv', number_of_folds=5, paramter_range_divider=300,
                                                        parameter_starting_point=4,
                                                        step_size=3,
                                                        number_of_params=5,
                                                        plot_results=True,
                                                        print_results=False):
    """
    Runs the experiment. To run this function and get the expected graph, simply run it with the default parameters.
    :param test_str:                 Path for the test set csv file.
    :param number_of_folds:          Sets the number of different folds we will train and test over.
    :param paramter_range_divider:   This number defines how many different params are available, depending on the
                                     test set size.
    :param parameter_starting_point: This parameter defines in which of the fragments set by "parameter_range_divider"
                                     we will set the first 'm' param over.
    :param step_size:                Sets the step from param to param in multiplications of the fragments set
                                     by "parameter_range_divider".
    :param plot_results:             Bool, marks whether we should use matplotlib to plot the results.
    :param print_results             Bool, marks whether we should print the results in text format.
    :return:
    """
    if paramter_range_divider < number_of_folds or\
            ((paramter_range_divider - parameter_starting_point) // step_size) < number_of_folds:
        return
    examples, np_features = get_set(test_str)
    fragment = (len(examples) // (paramter_range_divider))
    m_params = np.arange(start=(fragment * parameter_starting_point),
                         stop=((fragment * (step_size * (number_of_params - 1) + parameter_starting_point)) + 1),
                         step=(fragment * step_size))
    kfold = KFold(n_splits=number_of_folds, shuffle=True, random_state=313177784)
    m_results_all_folds = []  #Holds all results for each fold.
    i = 1
    for train_index, test_index in kfold.split(examples):
        current_fold_examples = [examples[i] for i in train_index]
        current_fold_tests = [examples[i] for i in test_index]
        m_results_current_fold = []
        for m in m_params:
            id3_pruning = ID3_Pruning(examples=current_fold_examples, features=np_features, pruning_param=m)
            classifier = id3_pruning.create_decision_tree()
            preciseness = run_test(classifier=classifier, test_set=current_fold_tests, print_nothing=True)
            m_results_current_fold.append((m, preciseness))
        m_results_all_folds.append((i, m_results_current_fold))
        i += 1


    sum_results_all_folds = {}
    for m in m_params:
        sum_results_all_folds[m] = 0

    for fold_res in m_results_all_folds:
        for res_tup in fold_res[1]:
            sum_results_all_folds[res_tup[0]] += res_tup[1]

    average_results_out_of_folds = [(k, (v / number_of_folds)) for k, v in sum_results_all_folds.items()]

    if print_results:
        print("A list of results, where each result is a tuple of (parameter, average preciseness):")
        print(average_results_out_of_folds)

    if plot_results:
        X = []
        Y = []
        for tup in average_results_out_of_folds:
            X.append(tup[0])
            Y.append(tup[1])
        plt.plot(X, Y)
        plt.ylabel('Preciseness')
        plt.xlabel('Pruning parameter')
        plt.show()

"""
//////////////////////////////////////    Q3 PART 3   /////////////////////////////////////////
"""
"""
experiment(test_str='train.csv',
           number_of_folds=5,
           paramter_range_divider=300,
           parameter_starting_point=4,
           step_size=3,
           number_of_params=5,
           plot_results=True,
           print_results=False)
"""
"""
//////////////////////////////////////    Q3 PART 4   /////////////////////////////////////////
"""

def question_3(dont_print_info=True, optimal_pruning_parameter=7):
    not dont_print_info and print("Building a training set.")

    train_examples, np_features = get_set('train.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Creating the ID3 class:")

    pruning_id3 = ID3_Pruning(examples=train_examples, features=np_features, pruning_param=optimal_pruning_parameter)

    not dont_print_info and print("Done.")
    not dont_print_info and print("Training a classifier:")

    import time
    start_time = time.time()

    classifier = pruning_id3.create_decision_tree()

    not dont_print_info and print("Finished training after", time.time() - start_time, "seconds.")
    not dont_print_info and print("Done.")

    not dont_print_info and print("Building a set of examples from the training group.")

    train_test_examples, _ = get_set('train.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Will now test the training examples.")

    not dont_print_info and run_test(classifier=classifier, test_set=train_test_examples, test_str="training subjects", positive_val='M')

    not dont_print_info and print("Building a set of examples from the test group.")

    test_examples, _ = get_set('test.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Running classifications on all examples...")

    run_test(classifier=classifier, test_set=test_examples, test_str="test subjects", positive_val='M',
             print_only_preciseness=dont_print_info)

#  question_3(dont_print_info=True, optimal_pruning_parameter=7)

"""
///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////   QUESTION 4   /////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
"""

def calculate_loss(classifier: DecisionTree, test_set: list, positive_val='M', weight=10):
    (correct_classifications,
     false_classifications,
     correct_positive_classifications,
     false_positive_classifications,
     correct_negative_classifications,
     false_negative_classifications) = get_results(classifier=classifier,
                                                   test_set=test_set,
                                                   positive_val=positive_val)
    return ((1 / weight) * false_positive_classifications + false_negative_classifications) / len(test_set)


def run_test_with_loss(classifier: DecisionTree, test_set: list, test_str=None, positive_val='M',
                       print_only_preciseness=True, print_nothing=False):
    if print_nothing:
        print_only_preciseness = True

    loss_result = calculate_loss(classifier=classifier,
                                 test_set=test_set,
                                 positive_val=positive_val)

    if print_only_preciseness:
        if not print_nothing:
            print(loss_result)
        return loss_result

    print("Done.")
    print("")
    print("----------------------------------")
    print("Loss report -", test_str, ":")
    print("----------------------------------")
    print("- Loss:", loss_result)
    print("----------------------------------")
    print("")

    return loss_result


def question_4():
    train_examples, np_features = get_set('train.csv')
    pruning_id3 = ID3_Pruning(examples=train_examples, features=np_features, pruning_param=optimal_pruning_parameter)
    classifier = pruning_id3.create_decision_tree()
    test_examples, _ = get_set('test.csv')
    run_test_with_loss(classifier=classifier, test_set=test_examples, test_str="q4.1", positive_val='M',
                       print_only_preciseness=True, print_nothing=False)


#  question_4()
