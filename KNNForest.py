import numpy as np
from collections import Counter
import ID3
import random
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import KFold
import csv

best_n = 250
best_k = 50
best_p = 0.3


class ForestClassifier:
    """
    A classifier created using KNNDecisionForest.
    """
    def __init__(self, trees: list, centroids, k):
        self.trees = trees
        self.centroids = centroids
        self.k = k

    def distance(self, features: np.array, centroid):
        pow_sum = 0
        for i in range(len(features)):
            pow_sum += np.power((features[i] - centroid[i]), 2)
        return np.sqrt(pow_sum)

    def classify(self, test_subject: ID3.Example):
        trees_and_dist_from_subject = [(self.trees[i], self.distance(test_subject.features, self.centroids[i]))
                                       for i in range(len(self.trees))]
        sorted_trees_tups = sorted(trees_and_dist_from_subject, key=(lambda tup: tup[1]))
        sorted_k_trees = [sorted_trees_tups[i][0] for i in range(self.k)]
        k_classifications = [classifier.classify(test_subject=test_subject) for classifier in sorted_k_trees]
        classification_hist = Counter(k_classifications)
        return classification_hist.most_common()[0][0]


class KNNDecisionForest:
    """
        KNN Forest.
    """

    def __init__(self, examples: list, features: np.array, n, k, p,
                 default_class_chooser=ID3.Majority(),
                 feature_chooser=ID3.DynamicContinuousFeaturing(),
                 end_checker=ID3.PruneBySize(ID3.optimal_pruning_parameter)):
        """
        :param examples: Training set.
        :param features: Numpy array holding all feature indices.
        :param: n: size of the forest.
        :param: k: Amount of trees used for each classification.
        :param: p: a float sized between 0.3 and 0.7. Determines the size of the training set for each decision tree.
        :param: default_class_chooser: an object of class ClassChooser. Is in charge of choosing a classification
                when a leaf is created.
        :param: feature_chooser: an object of class FeatureChooser. Is in charge of choosing a feature for any
                Node our of all available features.
        :param: end_checker: an object of class EndChecker. Its "finished" method returns True if it is time to
                create a leaf type node.
        """

        self.examples = examples.copy()
        self.features = features.copy()

        self.default_class_chooser = default_class_chooser
        self.feature_chooser = feature_chooser
        self.end_checker = end_checker

        self.n = n
        self.k = k
        self.tree_size = round(p * n)

    def create_centroid(self, examples):
        # Each element is a list of features:
        feature_arrays = [e.features for e in examples]
        features_sum = [0] * len(self.features)
        for i in range(len(self.features)):
            for j in range(len(examples)):
                features_sum[i] += feature_arrays[j][i]
        centroid = [(value / len(examples)) for value in features_sum]
        return centroid

    def create_decision_forest(self):
        trees = []
        centroids = []
        shuffling_examples = self.examples.copy()
        for i in range(self.n):
            random.shuffle(shuffling_examples)
            tree_examples = shuffling_examples[:self.tree_size].copy()
            centroid = self.create_centroid(tree_examples)

            tdidt = ID3.TDIDT(examples=tree_examples, features=self.features,
                              default_class_chooser=self.default_class_chooser,
                              feature_chooser=self.feature_chooser,
                              end_checker=self.end_checker)
            tree_classifier = tdidt.create_decision_tree()

            trees.append(tree_classifier)
            centroids.append(centroid)

        return ForestClassifier(trees=trees, centroids=centroids, k=self.k)


def get_tree_results(train_str='train.csv', test_str='test.csv', p=0.3, n=50, k=10,
                     default_class_chooser=ID3.Majority(),
                     feature_chooser=ID3.DynamicContinuousFeaturing(),
                     end_checker=ID3.PruneBySize(ID3.optimal_pruning_parameter)):
    train_examples, np_features = ID3.get_set(train_str)
    knn_forest = KNNDecisionForest(examples=train_examples, features=np_features, n=n, k=k, p=p,
                                   default_class_chooser=default_class_chooser,
                                   feature_chooser=feature_chooser,
                                   end_checker=end_checker)
    classifier = knn_forest.create_decision_forest()
    test_examples, _ = ID3.get_set(test_str)
    return ID3.get_results(classifier=classifier, test_set=test_examples, positive_val='M')


def get_tree_group_results(train_set, test_set, np_features, p=0.3, n=50, k=10,
                     default_class_chooser=ID3.Majority(),
                     feature_chooser=ID3.DynamicContinuousFeaturing(),
                     end_checker=ID3.PruneBySize(ID3.optimal_pruning_parameter)):
    knn_forest = KNNDecisionForest(examples=train_set, features=np_features, n=n, k=k, p=p,
                                   default_class_chooser=default_class_chooser,
                                   feature_chooser=feature_chooser,
                                   end_checker=end_checker)
    classifier = knn_forest.create_decision_forest()
    return ID3.get_results(classifier=classifier, test_set=test_set, positive_val='M')


def forest_kfold_experiment(p_steps=0.1, k_coef=5, n_min_range=50, n_max_range=500, n_steps=100):
    """
    This function prints the experiment results to a file.
    If you wish to run this experiment, know that it takes a long while to do so. Please take care in setting up the
    parameters such that you won't have any python errors as the function is heading towards completion.
    :param: p_steps: sets up the step size for p between 0.3 to 0.7
    :param: k_coef:  K params are set according to N size. This coefficient determines the fraction to pull from N to
                     create K, and to iterate over while testing.
    :param: n_min and n_max _range: The range to check for N.
    :param: n_steps:                The step size to check N for. Determines how many different N params you will check.
    """
    p_params = np.arange(start=0.3, stop=0.701, step=p_steps)
    n_params = np.arange(start=n_min_range, stop=n_max_range + 1, step=n_steps)
    param_tuples = []
    for p in p_params:
        for n in n_params:
            k_step_size = n // k_coef
            k_params = np.arange(start=k_step_size, stop=n + 1, step=k_step_size)
            for k in k_params:
                param_tuples.append((p, (n, k)))

    file = open('tree_experiment_results.csv', 'w', newline='')
    writer = csv.writer(file)
    writer.writerow(['p', 'N', 'K', 'Test result'])

    train_examples, np_features = ID3.get_set('train.csv')

    kfold = KFold(n_splits=5, shuffle=True, random_state=313177784)
    m_results_all_folds = []  # Holds all results for each fold.
    i = 1
    for train_index, test_index in kfold.split(train_examples):
        current_fold_examples = [train_examples[i] for i in train_index]
        current_fold_tests = [train_examples[i] for i in test_index]
        m_results_current_fold = []
        for tup in param_tuples:
            preciseness = get_tree_group_results(train_set=current_fold_examples, test_set=current_fold_tests,
                                                    np_features=np_features, p=tup[0], n=tup[1][0], k=tup[1][1],
                                                    default_class_chooser=ID3.Majority(),
                                                    feature_chooser=ID3.DynamicContinuousFeaturing(),
                                                    end_checker=ID3.PruneBySize(ID3.optimal_pruning_parameter))
            m_results_current_fold.append((tup, preciseness))
        m_results_all_folds.append((i, m_results_current_fold))
        i += 1

    sum_results_all_folds = {}
    for tup in param_tuples:
        sum_results_all_folds[tup] = 0

    for fold_res in m_results_all_folds:
        for res_tup in fold_res[1]:
            sum_results_all_folds[res_tup[0]] += res_tup[1]

    average_results_out_of_folds = [(k, (v / 5)) for k, v in sum_results_all_folds.items()]

    for tup in average_results_out_of_folds:
        results_row = [tup[0][0], tup[0][1][0], tup[0][1][1], tup[1]]
        writer.writerow(results_row)


#  forest_kfold_experiment()


def get_single_result(results_path='experiment_results.csv', p=0.3, n=450, k=10):
    results = pd.read_csv(results_path)
    res_list = []
    for index, row in results.iterrows():
        if row[1] == n:
            res_list.append(row[3])
        #  if row[0] == p and row[1] == n and row[2] == k:
        #      return row[3]
    return res_list


def f(x, y):
    return get_single_result(k=(np.round(y * x)), p=y)


def plot_results(n=450):
    """
    Plots the results of the above experiment. Take notice - you must fix the param range according to the above
    function parameters, if you changed them.
    :param: n: The current N size to print the graph for. Must also be changed in function "get_single_result".
    """
    k_coef = 5
    k_step_size = n // k_coef

    p_params = np.arange(start=0.3, stop=0.701, step=0.1)
    k_params = np.arange(start=k_step_size, stop=n + 1, step=k_step_size)

    X, Y = np.meshgrid(k_params, p_params)

    temp_Z = np.array(f(X, Y))
    Z = []
    i = 0
    temp_list_z = []
    for z in temp_Z:
        temp_list_z.append(z)
        i += 1
        if i == 5:
            i = 0
            Z.append(temp_list_z.copy())
            temp_list_z = []
    Z = np.array(Z)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X,Y,Z)
    ax.set_xlabel('K')
    ax.set_ylabel('p')
    ax.set_zlabel('Preciseness result for N=' + str(n));
    plt.show()


#  plot_results()


def question_6(dont_print_info=True):
    not dont_print_info and print("Building a training set.")

    train_examples, np_features = ID3.get_set('train.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Creating the KNNForest class:")

    knn_forest = KNNDecisionForest(examples=train_examples, features=np_features, n=best_n, k=best_k, p=best_p)

    not dont_print_info and print("Done.")
    not dont_print_info and print("Training a classifier:")

    import time
    start_time = time.time()

    classifier = knn_forest.create_decision_forest()

    not dont_print_info and print("Finished training after", time.time() - start_time, "seconds.")
    not dont_print_info and print("Done.")

    not dont_print_info and print("Building a set of examples from the training group.")

    train_test_examples, _ = ID3.get_set('train.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Will now test the training examples.")

    not dont_print_info and ID3.run_test(classifier=classifier, test_set=train_test_examples, test_str="training subjects", positive_val='M')

    not dont_print_info and print("Building a set of examples from the test group.")

    test_examples, _ = ID3.get_set('test.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Running classifications on all examples...")

    ID3.run_test(classifier=classifier, test_set=test_examples, test_str="test subjects", positive_val='M',
                 print_only_preciseness=dont_print_info)


if __name__ == '__main__':
    question_6()
