import numpy as np
from collections import Counter
import ID3
import random
import csv
from sklearn.model_selection import KFold
import pandas as pd
import matplotlib.pyplot as plt
best_n = 250
best_k = 50
best_p = 0.3
best_n_of_conferences = 5
best_conference_size = 25
best_overfitting_percentage = 0.3


class ImprovedForestClassifier:
    """
    A classifier created using KNNDecisionForest.
    """
    def __init__(self, trees: list, centroids, k, max_features, min_features,
                 used_feature_indices, number_of_conferences, size_of_conference):
        self.trees = trees
        self.centroids = centroids
        self.k = k
        self.max_features = max_features
        self.min_features = min_features
        self.used_feature_indices = used_feature_indices
        self.number_of_conferences = number_of_conferences
        self.size_of_conference = size_of_conference

    def normalize_feature(self, feature_index, feature_value):
        max_val = max(self.max_features[feature_index], feature_value)
        min_val = min(self.min_features[feature_index], feature_value)
        return (feature_value - min_val) / (max_val - min_val)

    def distance(self, features: np.array, centroid):
        pow_sum = 0
        for i in self.used_feature_indices:
            pow_sum += np.power((self.normalize_feature(i, features[i]) - self.normalize_feature(i, centroid[i])), 2)
        return np.sqrt(pow_sum)

    def classify(self, test_subject: ID3.Example):
        trees_and_dist_from_subject = [(self.trees[i], self.distance(test_subject.features, self.centroids[i]))
                                       for i in range(len(self.trees))]
        sorted_trees_tups = sorted(trees_and_dist_from_subject, key=(lambda tup: tup[1]))
        sorted_k_trees = [sorted_trees_tups[i][0] for i in range(self.k)]
        all_conference_classifications = []
        for i in range(self.number_of_conferences):
            shuffled_k_trees = sorted_k_trees.copy()
            random.shuffle(shuffled_k_trees)
            tree_conference = [shuffled_k_trees[i] for i in range(self.size_of_conference)]
            conference_classifications = [classifier.classify(test_subject=test_subject) for classifier in tree_conference]
            conference_hist = Counter(conference_classifications)
            all_conference_classifications.append(conference_hist.most_common()[0][0])
        classification_hist = Counter(all_conference_classifications)
        return classification_hist.most_common()[0][0]


class ImprovedKNNDecisionForest:
    """
        Improved KNN Forest.
    """

    def __init__(self, examples: list, features: np.array, n, k, p, number_of_conferences, size_of_conference,
                 default_class_chooser=ID3.Majority(),
                 feature_chooser=ID3.DynamicContinuousFeaturing(),
                 end_checker=ID3.PruneBySize(ID3.optimal_pruning_parameter),
                 overfitting_percentage=0.2,
                 overfitting_default_class_chooser=ID3.Majority(),
                 overfitting_feature_chooser=ID3.DynamicContinuousFeaturing(),
                 overfitting_end_checker=ID3.SingleClassEnd()):
        """
        :param examples: Training set.
        :param features: Numpy array holding all feature indices.
        :param: n: size of the forest.
        :param: k: Amount of trees used for each classification.
        :param: p: a float sized between 0.3 and 0.7. Determines the size of the training set for each decision tree.
        :param: number_of_conferences: How many individual sub conferences will be created out of the K trees selected
                                       from the N sized forest.
        :param: size_of_conference: The size of each individual sub conference.
        Note: Setting number_of_conferences=1 and size_of_conference=K will operate with a single conference sized
              K, like a regular KNNForest classifier.
        :param: default_class_chooser: an object of class ClassChooser. Is in charge of choosing a classification
                when a leaf is created.
        :param: feature_chooser: an object of class FeatureChooser. Is in charge of choosing a feature for any
                Node our of all available features.
        :param: end_checker: an object of class EndChecker. Its "finished" method returns True if it is time to
                create a leaf type node.
        :param: overfitting_percentage: How much out of N will be created using the overfitting classifier params.
        :param: overfitting_default_class_chooser: ClassChooser for the overfitting percentage of the created forest.
        :param: overfitting_feature_chooser: FeatureChooser for the overfitting percentage of the created forest.
        :param: overfitting_end_checker: EndChecker for the overfitting percentage of the created forest.
        """

        self.examples = examples.copy()
        self.features = features.copy()

        self.default_class_chooser = default_class_chooser
        self.feature_chooser = feature_chooser
        self.end_checker = end_checker

        self.n = n
        self.k = k
        self.tree_size = round(p * n)

        self.overfitting_group_size = round(n * overfitting_percentage)
        self.overfitting_default_class_chooser = overfitting_default_class_chooser
        self.overfitting_feature_chooser = overfitting_feature_chooser
        self.overfitting_end_checker = overfitting_end_checker

        self.number_of_conferences = number_of_conferences
        self.size_of_conference = size_of_conference

    def create_centroid(self, examples):
        # Each element is a list of features:
        feature_arrays = [e.features for e in examples]
        features_sum = [0] * len(self.features)
        for i in range(len(self.features)):
            for j in range(len(examples)):
                features_sum[i] += feature_arrays[j][i]
        centroid = [(value / len(examples)) for value in features_sum]
        return centroid

    def get_feature_list_recursive(self, tree_node: ID3.DecisionTreeNode, feature_indices_list):
        if tree_node.classification is not None:
            return
        feature_index = tree_node.feature.index
        feature_indices_list.append(feature_index)

    def get_used_features(self, tree_classifiers):
        feature_indices_list = []
        for tree in tree_classifiers:
            self.get_feature_list_recursive(tree.root, feature_indices_list)
        return set(feature_indices_list)

    def create_decision_forest(self):
        trees = []
        centroids = []
        shuffling_examples = self.examples.copy()

        for i in range(self.overfitting_group_size):
            random.shuffle(shuffling_examples)
            tree_examples = shuffling_examples[:self.tree_size].copy()
            centroid = self.create_centroid(tree_examples)

            tdidt = ID3.TDIDT(examples=tree_examples, features=self.features,
                              default_class_chooser=self.overfitting_default_class_chooser,
                              feature_chooser=self.overfitting_feature_chooser,
                              end_checker=self.overfitting_end_checker)
            tree_classifier = tdidt.create_decision_tree()

            trees.append(tree_classifier)
            centroids.append(centroid)

        for i in range(self.n - self.overfitting_group_size):
            random.shuffle(shuffling_examples)
            tree_examples = shuffling_examples[:self.tree_size].copy()
            centroid = self.create_centroid(tree_examples)

            tdidt = ID3.TDIDT(examples=tree_examples, features=self.features,
                              default_class_chooser=self.default_class_chooser,
                              feature_chooser=self.feature_chooser,
                              end_checker=self.end_checker)
            tree_classifier = tdidt.create_decision_tree()

            trees.append(tree_classifier)
            centroids.append(centroid)

        used_feature_indices = self.get_used_features(tree_classifiers=trees)
        max_features = [-np.inf] * len(self.features)
        min_features = [np.inf] * len(self.features)
        for e in self.examples:
            for i in range(len(self.features)):
                max_features[i] = max(max_features[i], e.features[i])
                min_features[i] = min(min_features[i], e.features[i])

        return ImprovedForestClassifier(trees=trees, centroids=centroids, k=self.k,
                                        max_features=max_features, min_features=min_features,
                                        used_feature_indices=used_feature_indices,
                                        number_of_conferences=self.number_of_conferences,
                                        size_of_conference=self.size_of_conference)


def get_improved_forest_group_results(train_set, test_set, np_features, number_of_conferences, size_of_conference, overfitting_percentage):
    improved_knn_forest = ImprovedKNNDecisionForest(examples=train_set, features=np_features,
                                                    n=best_n, k=best_k, p=best_p,
                                                    number_of_conferences=number_of_conferences,
                                                    size_of_conference=size_of_conference,
                                                    overfitting_percentage=overfitting_percentage)
    classifier = improved_knn_forest.create_decision_forest()
    results = ID3.get_results(classifier=classifier, test_set=test_set, positive_val='M')
    return results[0] / len(test_set)


def improved_forest_kfold_experiment():
    """
    Same as the file before, experiment set on different param values. In order to run the experiment detailed in my
    report document, simply run this function, and receive a corresponding CSV file with the results.
    """
    overfitting_params = np.arange(start=0.3, stop=0.401, step=0.1)
    number_of_conferences_params = np.arange(start=3, stop=6, step=2)
    param_tuples = []
    for o_param in overfitting_params:
        for conf_num_param in number_of_conferences_params:
            size_start = round(0.5 * best_k)
            size_stop = round(0.7 * best_k) + 1
            size_step = round(0.2 * best_k)
            conference_size_params = np.arange(start=size_start, stop=size_stop, step=size_step)

            for conf_size_param in conference_size_params:
                param_tuples.append((o_param, (conf_num_param, conf_size_param)))

    file = open('temp_improved_experiment_results.csv', 'w', newline='')
    writer = csv.writer(file)
    writer.writerow(['Overfitting percentage', 'Number of sub-conferences', 'Sub conference size', 'Test result', 'Fold'])
    file.close()

    file = open('improved_experiment_results.csv', 'w', newline='')
    writer = csv.writer(file)
    writer.writerow(['Overfitting percentage', 'Number of sub-conferences', 'Sub conference size', 'Test result'])

    temp_train_examples, np_features = ID3.get_set('train.csv')
    temp_test_examples, _ = ID3.get_set('test.csv')
    train_examples = temp_train_examples + temp_test_examples

    kfold = KFold(n_splits=5, shuffle=True, random_state=313177784)
    m_results_all_folds = []  # Holds all results for each fold.
    i = 1
    for train_index, test_index in kfold.split(train_examples):
        current_fold_examples = [train_examples[i] for i in train_index]
        current_fold_tests = [train_examples[i] for i in test_index]
        m_results_current_fold = []
        for tup in param_tuples:
            preciseness = get_improved_forest_group_results(train_set=current_fold_examples, test_set=current_fold_tests,
                                                            np_features=np_features, overfitting_percentage=tup[0],
                                                            number_of_conferences=tup[1][0], size_of_conference=tup[1][1])
            m_results_current_fold.append((tup, preciseness))
            print("Finished calculating results for", tup, "in fold number", i, "the result is:", preciseness)
            temp_file = open('temp_improved_experiment_results.csv', 'a', newline='')
            temp_writer = csv.writer(temp_file)
            temp_results_row = [tup[0], tup[1][0], tup[1][1], preciseness, i]
            temp_writer.writerow(temp_results_row)
            temp_file.close()

        m_results_all_folds.append((i, m_results_current_fold))
        i += 1

    sum_results_all_folds = {}
    for tup in param_tuples:
        sum_results_all_folds[tup] = 0

    for fold_res in m_results_all_folds:
        for res_tup in fold_res[1]:
            sum_results_all_folds[res_tup[0]] += res_tup[1]

    average_results_out_of_folds = [(k, (v / 5)) for k, v in sum_results_all_folds.items()]

    for tup in average_results_out_of_folds:
        results_row = [tup[0][0], tup[0][1][0], tup[0][1][1], tup[1]]
        writer.writerow(results_row)


#  improved_forest_kfold_experiment()


def get_single_result(conf_num, conf_size, results_path='improved_experiment_results.csv', o_param=0.2):
    results = pd.read_csv(results_path)
    for index, row in results.iterrows():
        if row[0] == o_param and row[1] == conf_num and row[2] == conf_size:
            return row[3]


def f(x, y):
    return get_single_result(conf_num=x, conf_size=y)


def get_all_results(o_param=0.2, results_path='improved_experiment_results.csv'):
    results = pd.read_csv(results_path)
    result_list = []
    for index, row in results.iterrows():
        if row[0] == o_param:
            result_list.append(row[3])
    return result_list


def plot_results(o_param=0.3):
    """
    Run this function if you wish to view the results for the above experiment in a matplotlib graph.
    The above experiment creates two graphs, one for 0.3 and one for 0.4. Set up o_param according
    to the one you wish to view.
    """
    number_of_conferences_params = np.arange(start=3, stop=6, step=2)
    size_start = round(0.5 * best_k)
    size_stop = round(0.7 * best_k) + 1
    size_step = round(0.2 * best_k)
    conference_size_params = np.arange(start=size_start, stop=size_stop, step=size_step)

    X, Y = np.meshgrid(conference_size_params, number_of_conferences_params)

    temp_Z = np.array(get_all_results(o_param=o_param))

    Z = []
    i = 0
    temp_list_z = []
    for z in temp_Z:
        temp_list_z.append(z)
        i += 1
        if i == 2:
            i = 0
            Z.append(temp_list_z.copy())
            temp_list_z = []

    Z = np.array(Z)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X, Y, Z)
    ax.set_xlabel('Conference size (p\' * K)')
    ax.set_ylabel('Number of sub-conferences (H)')
    ax.set_zlabel('Preciseness result for overfitting percentage =' + str(o_param));
    plt.show()


#  plot_results(o_param=0.4)


def question_7(dont_print_info=True):
    not dont_print_info and print("Building a training set.")

    train_examples, np_features = ID3.get_set('train.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Creating the KNNForest class:")

    knn_forest = ImprovedKNNDecisionForest(examples=train_examples, features=np_features,
                                           n=best_n, k=best_k, p=best_p,
                                           number_of_conferences=best_n_of_conferences,
                                           size_of_conference=best_conference_size,
                                           overfitting_percentage=best_overfitting_percentage)

    not dont_print_info and print("Done.")
    not dont_print_info and print("Training a classifier:")

    import time
    start_time = time.time()

    classifier = knn_forest.create_decision_forest()

    not dont_print_info and print("Finished training after", time.time() - start_time, "seconds.")
    not dont_print_info and print("Done.")

    not dont_print_info and print("Building a set of examples from the training group.")

    train_test_examples, _ = ID3.get_set('train.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Will now test the training examples.")

    not dont_print_info and ID3.run_test(classifier=classifier, test_set=train_test_examples, test_str="training subjects", positive_val='M')

    not dont_print_info and print("Building a set of examples from the test group.")

    test_examples, _ = ID3.get_set('test.csv')

    not dont_print_info and print("Done.")
    not dont_print_info and print("Running classifications on all examples...")

    ID3.run_test(classifier=classifier, test_set=test_examples, test_str="test subjects", positive_val='M',
                 print_only_preciseness=dont_print_info)


if __name__ == '__main__':
    question_7()
